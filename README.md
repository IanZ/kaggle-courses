# Kaggle Courses

This project consists of docker containers and Jupyter notebooks for learning Kaggle data science courses.

# How to run
1. Go to each course sub-directory and run `docker-compose up` to start notebook server.
2. Follwe onscreen instruction and start using notebooks.

# References
[Jupyter Docker Stacks](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html)
[Kaggle Courses](https://www.kaggle.com/learn/overview)
